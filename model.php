<?php

   class Model {
      protected $pdo;
      protected $db_driver = 'mysql';
      protected $db_host = 'localhost';
      protected $db_user = 'root';
      protected $db_password = '';
      protected $db_database = 'rxi';
      protected $query;

      public function __construct() {
               try {
                  $this->pdo = new PDO($this->db_driver . ':host=' . $this->db_host . ';dbname=' . $this->db_database . '', $this->db_user, $this->db_password);
               } catch (Exception $e) {
                  echo 'Connection failed: ' . $e->getMessage();
               }
            }

      public function getAll() {
         $this->query = 'SELECT * FROM ' . $this->table;
         $stmt = $this->pdo->prepare($this->query);
         $stmt->execute();

         $results = $stmt->fetchAll();

         return $results;
      }

      public function getById($id, $columns) {
         $this->query = 'SELECT ' . $columns . ' FROM ' . $this->table . ' WHERE id=:id';
         $stmt = $this->pdo->prepare($this->query);
         $stmt->bindParam(':id', $id, PDO::PARAM_INT);
         $stmt->execute();

         $results = $stmt->fetch();

         return $results;
      }

      public function getByUsername($username, $columns) {
         $this->query = 'SELECT ' . $columns . ' FROM ' . $this->table . ' WHERE username=:username';
         $stmt = $this->pdo->prepare($this->query);
         $stmt->bindParam(':username', $username, PDO::PARAM_STR);
         $stmt->execute();

         $results = $stmt->fetch();

         return $results;
      }

      public function deleteById($id, $columns) {
         $this->query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';
         $stmt = $this->pdo->prepare($this->query);
         $stmt->bindParam(':id', $id, PDO::PARAM_INT);
         $stmt->execute();
      }

      public function insert($columns, $values) {
         $this->query = 'INSERT INTO ' . $this->table . '(' . $columns . ') VALUES (' . $values . ')';
         $stmt = $this->pdo->prepare($this->query);
         $stmt->execute();
      }

      public function getMaxId() {
         $this->query = 'SELECT max(id) FROM ' . $this->table;
         $stmt = $this->pdo->prepare($this->query);
         $stmt->execute();

         $results = $stmt->fetch();
         //var_dump($results);
         return $results;
      }

      public function update($values, $where, $params) {
         $query = 'UPDATE '. $this->table .' SET ' . $values .' WHERE ' . $where;

         $stmt = $this->pdo->prepare($query);
         foreach ($params as $key => &$val) {
            $stmt->bindParam(($key), $val, PDO::PARAM_STR);
         }
         $stmt->execute();
      }
   }
