<?php
   require 'functions.php';

   $user = new User();
   $error = '';
   $user_info = array();

   $id = $_POST['id'];
   $user_info[':first_name'] = htmlspecialchars($_POST['first_name']);
   $user_info[':last_name'] = htmlspecialchars($_POST['last_name']);
   $user_info[':email'] = $_POST['email'];
   $user_info[':phone'] = $_POST['phone'];
   $user_info[':username'] = htmlspecialchars($_POST['username']);
   $password = htmlspecialchars($_POST['password']);

   $user->updateUserInfo($id, $password, $user_info);
