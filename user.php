<?php
   class User extends Model {
      protected $table = 'Users';

      public function deleteUser($id) {
         $this->deleteById($id);
      }

      public function addNewUser() {
         $this->insert('id', 'DEFAULT');

         $max_id['new_id'] = $this->getMaxId();
         //var_dump($id);

         header('Content-type: application/json');
         die(json_encode($max_id));
      }

      public function updateUserInfo ($id, $password, $user_info) {

         if (filter_var($user_info[':email'], FILTER_VALIDATE_EMAIL) == false) {
            $error['err_status'] = 'valid email address';
         }

         if(preg_match("/^[0-9]{3}-?[0-9]{3}-?[0-9]{4}$/", $user_info[':phone']) == false){
            if(empty($error)) {
               $error['err_status'] = 'valid phone number';
            } else {
               $error['err_status'] = $error['err_status'] . ' and phone number';
            }
         }

         $id_check = $this->getByUsername($user_info[':username'], 'id');
         //echo $id_check['id'];
         //var_dump($id_check);
         if($id_check['id'] != $id && !empty($id_check['id'])) {
            if(empty($error)) {
               $error['err_status'] = 'unquie username';
            } else {
               $error['err_status'] = $error['err_status'] . ' and a unquie username';
            }
         }

         if(empty($error)) {
            if(empty($password)) {
               $this->update('first_name=:first_name, last_name=:last_name, email=:email, phone=:phone, username=:username',
            'id='. $id .'', $user_info);
            } else {
               $user_info[':password'] = md5($password);
               $this->update('first_name=:first_name, last_name=:last_name, email=:email, phone=:phone, username=:username, password=:password',
            'id='. $id .'', $user_info);
            }
            return;
         } else {
            header('Content-type: application/json');
            die(json_encode($error));
         }
      }
   }
