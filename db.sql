CREATE TABLE Users (
   `id`           INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
   `first_name`   VARCHAR(255),
   `last_name`    VARCHAR(255),
   `username`     VARCHAR(255),
   `password`     VARCHAR(255),
   `phone`        VARCHAR(15),
   `email`        VARCHAR(255),
   UNIQUE (`username`)
) ENGINE = MYISAM;

INSERT INTO `Users` (`id`, `first_name`, `last_name`, `username`, `password`, `phone`, `email`)
VALUES
	(1, 'William', 'Newberry', 'wberry', '123', '4015551234', 'wberry@rxinsider.com'),
	(2, 'Peter', 'Sugar', 'peters', '123', '4015552222', 'petersugar@rxinsider.com'),
	(3, 'Mike', 'Samson', 'mike123', '123', '5551234444', 'mike.samson@gmail.com'),
	(4, 'Sara', 'Smith', 'sara999', '123', '5554561234', 'smith.sara@university.mit.edu'),
	(5, 'Paul', 'Jones', 'pauljones', '123', '4014446666', 'pjones@url.edu'),
	(6, 'Carol', 'Jones', 'cjones', '123', '4017778888', 'cjones@cox.net'),
	(7, 'Susan', 'Smith', 'smithsue12', '123', '5161234444', 'smithsue@hr.company.com');
