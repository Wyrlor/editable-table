<?php
   require 'model.php';
   require 'user.php';

   function fillTable($edit = 0) {
      $user = new User();

      $users = $user->getAll();
      $editable = '';

      if($edit == 1) {
         $editable = ' contenteditable=true ';
      }

      foreach($users as $user) {
         echo '<tr>';
         echo '<form name="user_entry" method="post"><td name="id" class="id">' . $user['id'] . '</td>';
         echo '<td'.$editable.' name="first_name">' . $user['first_name'] . '</td>';
         echo '<td'.$editable.' name="last_name">' . $user['last_name'] . '</td>';
         echo '<td'.$editable.' name="email">' . $user['email'] . '</td>';
         echo '<td'.$editable.' name="phone">' . $user['phone'] . '</td>';
         echo '<td'.$editable.' name="username">' . $user['username'] . '</td>';
         //echo '<td'.$editable.'>' . $user['password'] . '</td>';
         echo '<td'.$editable.'  name="password"></td>';
         if($edit == 1) {
            echo '<td class="options"><span class="save_user">&#x2713;</span></td>';
            echo '<td class="options"><span class="delete_user">X</span></td></form>';
         }
         echo '</tr>';
      }
   }

   if(isset($_POST['user_entry_submit'])) {
      var_dump($_POST);
      exit();
   }
