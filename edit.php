<?php require 'functions.php'; ?>

<!DOCTYPE hmtl>
<html>
<head>
   <link href="includes/css/styles.css" rel="stylesheet" />
</head>
<body>
   <h1>Edit Table</h1>
   <h2>Click on a field to edit. Passwords will not be changed unless new text is entered.</h2>

   <div class="response_message">

   </div>

   <table id="user_table">
      <tr>
         <th>
            Id
         </th>
         <th>
            First Name
         </th>
         <th>
            Last Name
         </th>
         <th>
            Email
         </th>
         <th>
            Phone
         </th>
         <th>
            Username
         </th>
         <th>
            Password
         </th>
         <th>
            Save
         </th>
         <th>
            Delete
         </th>
      </tr>
      <?php fillTable(1); ?>
   </table>

   <a href="index.php"><input type="button" value="Index" /></a>
   <a class="add_user"><input type="button" value="Add User" /></a>

   <script src="includes/js/jquery.min.js"></script>
   <script>
      $(document).ready(function(argument) {
         $('.add_user').on('click', function(e) {
            e.PreventDefault;
            console.log('here');
            $.ajax({
               url: 'addUser.php',
               success: function(data) {
                  //console.log(data.new_id);
                  $('#user_table tr:last').after('<tr><td>' + data.new_id[0] + '</td><td></td><td></td><td></td><td></td><td></td><td></td><td class="options"><span class="save_user">&#x2713;</span></td><td class="options"><span class="delete_user">X</span></td></tr>');
               },
               error: function(data) {
                  console.log('error');
                  console.log(data);
               }
            });
         });
         $('.delete_user').on('click', function(e) {
            e.PreventDefault;

            console.log($(this).closest('tr').find('.id').html());
            var delete_id = $(this).closest('tr').find('.id').html();
            var $deleted = $(this).closest('tr');
            $.ajax({
               url: 'deleteUser.php',
               type: 'post',
               data: {id: delete_id},
               success: function(data) {
                  console.log('deleted');
                  console.log($(this).closest('tr'));
                  $deleted.remove();
               },
               error: function(data) {
                  console.log('error');
                  console.log(data);
               }
            });
         });
         $('.save_user').on('click', function() {
            var info = [];
            //console.log('clocked');
            var test = $(this).closest('tr').find('td');
            //console.log(test);
            $(this).closest('tr').find('td').each(function() {
               //console.log($(this).html());
               var key = $(this).attr('name'),
                   value = $(this).html();
               info[key] = value;
            });
            console.log(info['id']);
            $.ajax({
               url: 'updateInfo.php',
               type: 'post',
               data: {
                  id: info['id'],
                  first_name: info['first_name'],
                  last_name: info['last_name'],
                  email: info['email'],
                  phone: info['phone'],
                  username: info['username'],
                  password: info['password'],
               },
               success: function(data) {
                  console.log('success');
                  console.log('data:' + data);
                  //console.log(JSON.stringify(data));
                  if(data.err_status === undefined) {
                     show_success();
                  } else {
                     console.log(data.err_status);
                     show_failure(data.err_status);
                  }
               },
               error: function(data) {
                  console.log('error');
                  //console.log(data);
                  //data = JSON.stringify(data);
                  console.log(data);
                  console.log(JSON.encode(data.responseText));
                  show_failure(data.err_status);
               }
            });
         });
         function show_success() {
            var $this = $('.response_message');

            $this.css('background', '#31A83F');
            $this.html('Field has been successfully updated.');
            $this.fadeIn().delay(2000).fadeOut();

            $('td[name=password]').html('');
         }
         function show_failure(data) {
            var $this = $('.response_message');
            $this.css('background', '#A53232');
            $this.html('Please enter a ' + data);
            $this.fadeIn().delay(2000).fadeOut();
         }

      });
   </script>
</body>
</html>
