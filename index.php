<?php require 'functions.php'; ?>

<!DOCTYPE hmtl>
<html>
<head>
   <head>
      <link href="includes/css/styles.css" rel="stylesheet" />
   </head>
</head>
<body>
   <h1>Users Table</h1>
   <h2>Click edit to edit user records</h2>
   <table id="user_table">
      <tr>
         <th>
            Id
         </th>
         <th>
            First Name
         </th>
         <th>
            Last Name
         </th>
         <th>
            Email
         </th>
         <th>
            Phone
         </th>
         <th>
            Username
         </th>
         <th>
            Password
         </th>
      </tr>
      <?php fillTable(); ?>
   </table>

   <a href="edit.php"><input type="button" value="Edit" /></a>

   <script src="includes/js/jquery.min.js"></script>
</body>
</html>
